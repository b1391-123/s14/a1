

let helloWorld = `Hello World`;
console.log(helloWorld);

let firstName = `First Name: John`;
let lastName = `Last Name: Smith`;
let age = `Age: 30`;

console.log(firstName);
console.log(lastName);
console.log(age);

let hobbiesOne = "Hobbies:";
let hobbies = ["Biking", "Mountain Climbing", "Swimming"]
console.log(hobbiesOne);
console.log(hobbies);

let workAddressOne = "Work Address:";
let workAddress = {
	houseNumber: '32' ,
	street: 'Washington',
	city: 'Lincoln',
	state:'Nebraska'
}
console.log(workAddressOne);
console.log(workAddress);

let nameAge = "John Smith is 30 years of age."
let print = "This was printed inside printUserInfo function:"

console.log(nameAge);
console.log(workAddress);